<!--
This README.md is written in Markdown
For syntax reference, refer https://markdown-it.github.io/
Use following to convert this to html

# markdown README.md > README.html
-->

# Python Script to generate the IOdelay values
---


## IO delay configuration

When migrating to adapt to the IOdelay framework on DRA7xx SoCs,

* All the pinmuxing is performed from the bootloader
  * Allows to select the muxmode and pad direction
  * `LEGACY_MODE` padmux

* For some pads, along with mux mode, extra delay mode has to be configured
  * Allows to select one of the precoded delays from the ROM
  * `VIRTUAL_MODE` delay

* For some pads, additional registers have to be written with delays
  * Allows to manually set input/output delays per pad
  * `MANUAL_MODE` delay


## Description

This script is used for generating the padmux and IO delay data for the
existing use cases based on group selection of delay modes.

### Pinmux and iodelay
For each pad, depending on the muxmode that is being used, there can be
multiple choices of choosing the delay - LEGACY, VIRTUAL_MODE or MANUAL_MODE
For LEGACY_MODE, nothing special has to be done.
Depending on the way the pad is being used, a specific delay mode has to be chosen out of the multiple possibilities.

### Auto generation
This script uses the register dump of the PAD registers from a **running kernel**.
From this, it derives the **muxmode** and pad **directions** used in the system.
Now the only thing that the script needs to add is the delay information.

Using `PCT`(Pad Configuration Tool) XML files as reference, the script finds
out all the possible options for **VIRTUAL** and **MANUAL** mode delays.
Depending on the way the hardware is used, user has to choose a specific mode.

User has to select only the delay mode to be used by a PAD group so that the
script selects them whenever in conflict. Optionally, the script can run
interactively to build up this data via menu based selection.


## Files in this repo

| File | Description |
| ------ | ----------- |
| iodelay-autogen.py     | Python script |
| guidelines.txt         | Describes when a specific delay mode is to be used |
| XMLFiles/*.xml         | XML data from Pad Config Tool (Contains available delay modes and values)
| selected-modes.txt     | User selected delay modes to be used by script |
| ctrl-core.dump         | Register dump of all the PAD registers
| generate-all-modes.sh  | Helper script to generate all modes of a peripheral |


## Example usage

#### For getting the register dump of the PAD registers from "linux" system:-
* Run this command on target and save the output to a file called ctrl-core.dump
`omapconf dump 0x4a003400 0x4a00380c`
* Please refer to the usage and help message for more info.
`./iodelay-autogen.py --help`
---

#### For getting the register dump of the PAD registers from "bios" system:-
* For bios use cases, omapconf is not available. Use the CCS tool to
      create the dump of pad registers using gel script
  `get-pad-dump.gel`
---

#### For generating the IOdelay values from the old kernel interactively
* Run the script interactively with -i option (default).
* Select the delay mode for the PAD group in conflict. Refer to the guidelines.txt for description about that delay mode.
* For the peripherals in doubt, you can skip the entire PAD group
* Selection of each PAD group and the selected mode is printed at end.
* e.g. to generate the pad/delay data first time, run
  `./iodelay-autogen.py`
---

#### For generating the IOdelay values for a specific module only:-
* Use the module filter to specify the desired module. The script would only generate output wrt the module specified.
* e.g. to generate the pad/delay data only for VIN3A interface, run
  `./iodelay-autogen.py -m VIN3A`
---

#### For generating the IOdelay values for kernel format:-
* Use the format option -f to specify the output format.
* e.g. for generating pad/delay data for MMC1 in DTS format, run
  `./iodelay-autogen.py -m MMC1 -f linux`
---

#### For generating MMC data in kernel DTS format:-
* Use the generate-all-modes.sh helper similar to the python tool.
* e.g. for generating MMC1 for J6eco SR1.0, run
  `./generate-all-modes.sh -p dra72x -m MMC1 -r 1.0 -f linux`
---

#### For generating signal probe script for VIN1A signals:-
* Use the module filter for to specify the module.
* Enable '--gpio' option
  `./iodelay-autogen.py -m VIN1A --gpio`
* This would create vin1a-probe.sh which can be used on target

## Support and bugs
* Some PAD registers are hidden in the XML, so warnings for them are expected
* I2C1 PADs do not have mux mode names, so errors for them are expected

For any queries and support on this script, please write to
**Nikhil Devshatwar** <nikhil.nd@ti.com>
