#set -x
rm -f mode*.txt output.txt
PRM="$1 $2 $3 $4 $5 $6 $7 $8 -i 1"

echo 0 | ./iodelay-autogen.py $PRM > output.txt
cat output.txt | grep MENU > modes.txt
len=`cat modes.txt | wc -l`
len=$(($len - 3))

cat modes.txt
cat modes.txt > mode-all.txt
echo ">>> Found total modes $len"
for i in `seq 1 $len`
do
	name=`cat modes.txt | grep "MENU: $i:" | cut -d ' ' -f3`
	file="mode-$i-$name.txt"
	echo ">>> Generating for mode $name -> $file"
	echo $i | ./iodelay-autogen.py $PRM  > output.txt
	cat output.txt | grep -A1000 "########" > $file
	echo "## Auto generated pad/data for $name" >> mode-all.txt
	cat $file  >> mode-all.txt
done

rm -f output.txt
