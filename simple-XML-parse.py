#!/usr/bin/python
# Python script to experiment with XML parsing
# Author:- Nikhil Devshatwar

# Do not use this script to generate values for iodelay
# This is helpful to develop the iodelay-autogen.py whenever
# the XML file syntax changes

import xml.etree.ElementTree as ET
import re

XML_PATH = "XMLFiles/DRA72x"

pad_data_xml   = XML_PATH + "/CTRL_MODULE_CORE.xml"
iod_data_xml   = XML_PATH + "/IODELAYCONFIG.xml"
model_data_xml = XML_PATH + "/model_DRA72x_SR1.0_v1.0.9.xml"

pad_xml = ET.parse(pad_data_xml).getroot()
iod_xml = ET.parse(iod_data_xml).getroot()
model_xml = ET.parse(model_data_xml).getroot()

padlist = model_xml.findall("padDB/clockNode/type/pad")
