#!/usr/bin/python
# Python script to automatically generate the IO pad and delay data
# Author:- Nikhil Devshatwar

# This script uses regdump of the pad registers to figure out the muxmodes
# Based on the pad and muxmode, it find out all possible virtual/manual modes
# by referring to the PCT XML model. Using the, selection file, script
# decides which modes to be selected. Finally it dumps the required array entries

# This script depends on following files:-
# * XML data files	- Shipped with the DRA7xx PCT tool
# * Pad register dump	- Generate using omapconf dump 0x4a003400 0x4a00380c
# * selection file	- List of selected virtual/manual modes to be used

import xml.etree.ElementTree as ET
import re
import argparse

# Handle the command line arguments
parser = argparse.ArgumentParser(prog='iodelay-autogen.py',
	description='Python script to generate the IOdelay data.\n' \
		'This script refers to the XML data and automatically generates\n' \
		'PAD and DELAY data for the same use case.\n' \
		'Note that only the PADs which are used in the dump are configured.',
	epilog='Generate the pad and delay data using pad register dumps. For this,\n' \
		'Run omapconf dump 0x4a003400 0x4a00380c and save the output')

parser.add_argument('-p', '--part', dest='part',
	action='store', type=str, choices=["dra74x", "dra75x", "dra72x", "dra76x", "dra77x"], default="dra74x",
	help='select the device part')

parser.add_argument('-r', '--revision', dest='revision',
	action='store', type=str, choices=["1.0", "1.1", "2.0"], default="1.0",
	help='select the silicon revision')

parser.add_argument('-m', '--module', dest='module',
	action='store', type=str, default="",
	help='generate only for modules matching the provided RE')

parser.add_argument('-f', '--format', dest='format',
	action='store', type=str, choices=["linux", "uboot", "bios"], default="uboot",
	help='select the output format to be used')

parser.add_argument('-d', '--debug', dest='debug',
	action='store', type=int, choices=[0, 1, 2, 3, 4], default=0,
	help='set the debug level - ERR,WARN,INFO,DBG,VERBOSE')

parser.add_argument('-i', '--interactive', dest='interactive',
	action='store', type=int, choices=[0, 1], default=1,
	help='run interactively with menus for resolving conflicts')

parser.add_argument('-s', '--strict', dest='strict',
	action='store_true',
	help='strict mode - ask for each pad of the group, do not save selection.\n' \
		'For some peripherals, same delaymode cannot be used for all the pads.\n' \
		'Use this mode to select different modes for each pad')

parser.add_argument('-g', '--gpio', dest='gpio',
	action='store_true',
	help='generate script to probe signals using gpio')

parser.add_argument('-c', '--check', dest='check_xml',
	action='store_true',
	help='check consistency of the XML files and other data')

args = parser.parse_args()

# Some more knobs for developers, Don't want to expose them to cmd line
args.resetslew = True

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

VERSION = "2.8"

if (args.part == "dra74x" or args.part == "dra75x"):
	if (args.revision == "1.0" or args.revision == "1.1"):
		args.revision = "1.x"
	PART = "DRA75x_DRA74x"
	PCT_VERSION = "v1.0.16"
elif (args.part == "dra72x"):
	if (args.revision == "1.1"):
		print "dra72x doesn't have silicon revision 1.1"
		exit()
	PART = "DRA72x"
	PCT_VERSION = "v1.0.9"
elif (args.part == "dra76x" or args.part == "dra77x"):
	if (args.revision != "1.0"):
		print "%s doesn't have silicon revision %s" % (args.part, args.revision)
		exit()
	PART = "DRA77xP_DRA76xP"
	PCT_VERSION = "v1.0.1"

XML_PATH = "XMLFiles/" + PART
pad_data_xml   = XML_PATH + "/CTRL_MODULE_CORE.xml"
iod_data_xml   = XML_PATH + "/IODELAYCONFIG.xml"
model_data_xml = XML_PATH + "/model_" + PART + "_SR" + args.revision + "_" + PCT_VERSION + ".xml"
modehelp_file  = XML_PATH + "/guidelines_SR" + args.revision + ".txt"

pad_file = "ctrl-core.dump"
sel_file = "selected-modes.txt"

def trace(level, msg):
	if (args.debug >= level):
		print(msg)
def verbose(msg):
	trace(4, "DEBUG: " + msg)
def debug(msg, args=None):
	trace(3, "DEBUG: " + msg)
def info(msg):
	trace(2, "INFO: " + msg)
def warn(msg):
	trace(1, "WARN: " + msg)
def error(msg):
	trace(0, "ERR: " + msg)
def fatal(msg):
	trace(0, "FATAL: " + msg)
	exit()

if (args.interactive):
	print "iodelay-autogen.py - Python script to generate the IOdelay data."
	print "v" + VERSION + " using PCT version " + PCT_VERSION
	print "Parsing PCT data from " + model_data_xml + "..."
	print ""

# Read the XML file database for pad and delay registers
pad_xml = ET.parse(pad_data_xml).getroot()
iod_xml = ET.parse(iod_data_xml).getroot()
model_xml = ET.parse(model_data_xml).getroot()

# Functions to parse the input files and build data structures
def read_guidelines(fname):
	modehelp = {}
	pattern = re.compile('([^\t.]+\w)\t+(.+)')
	f = open(fname)
	for line in f.readlines():
		list = re.match(pattern, line)
		if ( list == None):
			continue
		mode = list.groups(0)[0]
		info = list.groups(0)[1]
		modehelp[mode] = info
		verbose("Guideline: Mode '%30s' => '%s'" % (mode, info))
	return modehelp

def read_selmodes(fname):
	sel = {}
	pattern = re.compile('(\w+)\W*,\W*(\w*)')
	f = open(fname)
	for line in f.readlines():
		list = re.match(pattern, line)
		if ( list == None):
			continue
		module = list.groups(0)[0]
		mode = list.groups(0)[1]
		sel[module] = mode
		verbose("Input modes: Select '%s' => '%s'" % (mode, module))
	return sel

def read_pad_dump(file):
	regmap = {}
	pattern = re.compile('.*(0x[0-9A-Fa-f]+).*(0x[0-9A-Fa-f]+).*')
	f = open(file, 'r')
	for line in f:
		list = re.match(pattern, line)
		if ( list == None):
			continue
		addr = int(list.groups(0)[0], 16)
		val = int(list.groups(0)[1], 16)
		verbose("XML pad-reg: Addr = %08x Value = %08x" % (addr, val))
		regmap[addr] = val
	return regmap

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Read all the input files and parse the required info
modehelp = read_guidelines(modehelp_file)
sel = read_selmodes(sel_file)
regmap = read_pad_dump(pad_file)
reglist = regmap.keys()

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Helper functions to parse XML nodes and get the desired data
# Hugely dependent on the XML format of the PCT files
# Uses XPath to search elements with queries and filters
# Parse the nodes to generate data in desired format

def xml_pad_get_name(offset):
	padlist = pad_xml.findall("register[@offset='0x%X']" % offset)
	if (len(padlist) == 0):
		return "Unknown"
	else:
		return padlist[0].get("id")

def xml_pad_get_offset(name):
	padlist = pad_xml.findall("register[@id='%s']" % name)
	if (len(padlist) == 0):
		return None
	else:
		return int(padlist[0].get("offset"), 16)

def xml_pad_get_slew(offset):
	padlist = pad_xml.findall("register[@offset='0x%X']" % offset)
	if (len(padlist) == 0):
		return 0x0
	else:
		bitlist = padlist[0].findall("bitfield[@begin='19']")
		if (len(bitlist) == 0):
			return 0x0
		else:
			return int(bitlist[0].get("resetval"), 16)

# For DRA72x SoC, for "ball compatibility" purpose, some signals have same muxmode.
# CTRL_CORE_ALT_SELECT_MUX register allows to select between one of the signals
# Its like a board mux has been added inside the SoC, except it uses same muxmode.
def resolve_group(muxmode_str):
	if (muxmode_str == "DRIVER OFF"):
		return muxmode_str
	mmlist = muxmode_str.split(" ")
	mmlen = len(mmlist)
	if (mmlen == 1):
		def_mode = mmlist[0]
		return mmlist[0]
	elif (mmlen == 2):
		def_mode = mmlist[0]
		grp1_mode = mmlist[1]
	elif (mmlen == 3):
		def_mode = mmlist[0]
		grp1_mode = mmlist[1]
		grp2_mode = mmlist[2]
	else:
		error("Cannot resolve muxmode group for '%s'" % muxmode_str)

	#HACK for now
	warn("Not resolving muxmode %s, assuming default %s" % (muxmode_str, def_mode))
	return def_mode

def xml_model_get_signal(offset, muxmode):
	padlist = pad_xml.findall("register[@offset='0x%X']" % offset)
	if (len(padlist) == 0):
		return "Unknown"
	else:
		mux_field = padlist[0].findall("./bitfield[@end='0']")
		if (len(mux_field) == 0):
			return "Unknown"
		modes = mux_field[0].findall("./bitenum[@value='%d']" % muxmode)
		if (len(modes) == 0):
			return "Unknown"
		return resolve_group(modes[0].get("id"))

def xml_iodelay_get_reg(name):
	delaylist = iod_xml.findall("register[@id='%s']" % name)
	if (len(delaylist) == 0):
		return 0
	else:
		offset = delaylist[0].get("offset")
		verbose("XML delay-reg: Name = %s Offset = %s" % (name, offset))
		return int(offset, 16)

def xml_get_virtual(mode):
	vmodes = {}
	signal = mode.findtext("signal")
	for virt in mode.findall("virtualmode/mode"):
		virt_mode = virt.findtext("value")
		virt_name = virt.findtext("name")
		if (virt_name == "NA"):
			continue
		virt_mode = int(virt_mode)
		vmodes[virt_name] = virt_mode
		debug("    * VIRTUAL: %s [%s] => delaymode = %d" % (signal, virt_name, virt_mode))
	return vmodes

def xml_get_manual(mode):
	mmodes = {}
	signal = mode.findtext("signal")
	for man in mode.findall("manualmode"):
		regname = man.findtext("cfgreg/name")
		for sel in man.findall("cfgreg/mode"):
			adel = sel.findtext("adelay")
			gdel = sel.findtext("gdelay")
			man_name = sel.findtext("name")
			if (man_name == "NA"):
				continue
			adel = int(adel)
			gdel = int(gdel)
			if (man_name not in mmodes.keys()):
				mmodes[man_name] = []
			mmodes[man_name].append((regname, adel, gdel))
			debug("    * MANUAL: %s [%s] => delay[%s] = %d, %d" % (signal, man_name, regname, adel, gdel))
	return mmodes

def xml_get_additional_mode(orig):
	additional_mux = orig.findall("additionalMux/confregisters")
	if (len(additional_mux) == 0):
		return orig
	else:
		additional_mux = additional_mux[0]
		mux_reg_name = additional_mux.findtext("register")
		offset = xml_pad_get_offset(mux_reg_name)

		if (offset == None):
			error("Could not find register address for register '%s'" % mux_reg_name)
			fatal("This pin may have virtual/manual modes, Fix XML file")

		addr = 0x4a002000 + offset
		if (addr not in reglist):
			error("register dump lacks data for additional muxes")
			warn("using default values")
			muxval = 0x0
		else:
			muxval = regmap[addr]

		values = additional_mux.findall("value")
		for value in values:

			option = int(value.findtext("option"))
			fieldpos = value.findtext("fieldPos")
			signal = value.findtext("signal").upper()
			matchlist = re.match("([0-9]+)[:]*([0-9]*)", fieldpos)

			verbose("++++ Additional mux %s possible" % signal)
			end = int(matchlist.groups(0)[0])
			if (matchlist.groups(0)[1] == ""):
				start = end
			else:
				start = int(matchlist.groups(0)[1])

			muxval = muxval & (1 << end)
			muxval = muxval >> start

			if (muxval == option):
				debug(">>>> Additional mux %s[%s] = %d" % (mux_reg_name, fieldpos, option))
				debug(">>>> Effective signal = %s" % signal)
				return value
		error("None of the additional Mux value matched, using default")
		return values[0]

def xml_find_delaymodes(pad_name, muxmode):
	virt = {}
	man = {}
	padlist = model_xml.findall("padDB/clockNode/type/pad")
	for pad in padlist:
		if (pad_name == pad.findtext("confregisters/regbit/register")):
			muxmodes = pad.findall("muxmode")
			break
	if not muxmodes:
		return None
	for mode in muxmodes:
		if ("%d" % muxmode == mode.findtext("mode")):
			mode = xml_get_additional_mode(mode)
			virt = xml_get_virtual(mode)
			man = xml_get_manual(mode)
			return (virt, man)
	return (virt, man)

def xml_check_correctness(modehelp):

	print "###########################################"
	print "XML correctness checks:"
	padcore_list = []
	padmodel_list = []
	modes = {}

	# Find all the pads in core XML
	pad_list = pad_xml.findall("register")
	for i in pad_list:
		name = i.get("id")
		if (re.match("CTRL_CORE_PAD_.*", name) == None):
			continue
		padcore_list.append(name)
	print ">> Total pads defined in core XML = %d" % len(padcore_list)

	# Find all the pads in model XML
	pad_list = model_xml.findall("padDB/clockNode/type/pad/confregisters/regbit")
	for i in pad_list:
		name = i.findtext("register")
		padmodel_list.append(name)
	print ">> Total pads defined in model XML = %d" % len(padmodel_list)

	# Find all the manual modes
	manmode_list = model_xml.findall("padDB/clockNode/type/pad/muxmode/manualmode/cfgreg/mode")
	for i in manmode_list:
		man_name = i.findtext("name")
		modes[man_name] = "manual"
	# Find all the additional manual modes
	manmode_list = model_xml.findall("padDB/clockNode/type/pad/muxmode/additionalMux/confregisters/value/manualmode/cfgreg/mode")
	for i in manmode_list:
		man_name = i.findtext("name")
		modes[man_name] = "manual"
	# Find all the virtual modes
	virtmode_list = model_xml.findall("padDB/clockNode/type/pad/muxmode/virtualmode/mode")
	for i in virtmode_list:
		virt_name = i.findtext("name")
		modes[virt_name] = "virtual"
	# Find all the additional virtual modes
	virtmode_list = model_xml.findall("padDB/clockNode/type/pad/muxmode/additionalMux/confregisters/value/virtualmode/mode")
	for i in virtmode_list:
		virt_name = i.findtext("name")
		modes[virt_name] = "virtual"
	print ">> Total delaymodes defined in model XML = %d" % len(modes)

	# Print out all the errors found in the tests
	print "== Pads defined in core XML but not in model XML =="
	for i in padcore_list:
		if (i not in padmodel_list):
			print "\t%s" % i
	print "== Pads defined in model XML but not in core XML =="
	for i in padmodel_list:
		if (i not in padcore_list):
			print "\t%s" % i

	# Check if description for each mode is available
	print "== Delaymodes missing description in the guidelines.txt =="
	for i in modes:
		if (i not in modehelp):
			print "\t%s" % i

	# Check if description for unknown mode is documented
	print "== Delaymodes unused from guidelines.txt =="
	for i in modehelp:
		if (i not in modes):
			print "\t%s" % i

	print "###########################################"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Decision point of the script
# For each pad/pin, decide what delay method is used
# Lookup in the selection file or ask user with a menu
def select_mode(pad, pin, module, virt, man, modehelp, sel):
	modelist = []
	for modename in virt.keys():
		modelist.append(modename)

	for modename in man.keys():
		modelist.append(modename)

	if (len(modelist) == 0):
		return "LEGACY"

	if (module in sel.keys()):
		mode = sel[module]
		if (mode == "SKIP" or mode == "LEGACY" or mode in modelist):
			return mode
		else:
			error("Invalid delaymode '%s' for module '%s'" % (mode, module))

	if (args.interactive == 0):
		warn("No delay modes for %s found, skipping" % module)
		mode = "SKIP"
		return mode

	# Display the menu and ask user to select the 'right' mode
	while True:
		print ""
		print "MENU: Select delay mode for %s -> %s" % (pad, pin)
		i = 0
		print "MENU: %d: %s \t\t\t%s" % (i, "SKIP", "Skips this module for now")
		i = 1
		print "MENU: %d: %s \t\t%s" % (i, "LEGACY", "No external delay config (No virtual/manual needed)")
		for mode in modelist:
			i += 1
			if (mode in modehelp):
				helptext = modehelp[mode]
			else:
				helptext = "Unknown"
			print "MENU: %d: %s \t\t%s" % (i, mode, helptext)
		try:
			choice = int(raw_input("Select delay mode #> "))
			if (choice == 0):
				mode = "SKIP"
				break
			elif (choice == 1):
				mode = "LEGACY"
				break
			elif (choice >=2 and choice <= len(modelist) + 1):
				mode = modelist[choice - 2]
				break
			print "ERROR: Invalid choice"
		except ValueError:
			print "ERROR: Invalid choice"
	print "MENU: Selected mode is %s" % mode
	print ""

	return mode
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Pad read using GPIO - Specific to linux only
# As each pad is muxed with a SoC gpio (for most of the pads)
# It's possible to read the pad data using gpio datain registers
# Change pad register to enable input path and export sysfs
# Use sysfs to read the status of GPIO and print in a loop
def pad_dump_gpio(padconf, per_padconf):
	gpio_data_addrs = (0, 0x4ae10138, 0x48055138, 0x48057138, 0x48059138, 0x4805b138, 0x4805d138, 0x48051138, 0x48053138)
	module = args.module

	# script header
	gen_header = "###########################################\n" + \
		     "# Read %s signals using GPIO sysfs\n" % module + \
		     "###########################################\n\n"
	gen_data = "%s_data=(\n" % module

	for i in padconf:
		(pad_name, pin_name, addr, val, mode, delayinfo) = i

		offset = addr - 0x4a002000
		gpio_name = xml_model_get_signal(offset, 14)
		matchlist = re.match("GPIO(.)_(.*)", gpio_name)
		if (matchlist == None):
			error("No GPIO for pad %s - cannot read from script" % pad_name)
			continue

		inst = int(matchlist.groups(0)[0])
		bit = int(matchlist.groups(0)[1])
		gpio_addr = gpio_data_addrs[inst]
		ngpio = (inst - 1) * 32 + bit
		pin_short = re.match(".+_(.*)", pin_name).groups(0)[0]

		gen_data += "\t%s:0x%x:%s:%d:0x%x:%d\n" % (pin_short, addr, gpio_name, ngpio, gpio_addr, bit)

	gen_data += ")\n\n"

	# Main script to probe the signal
	gen_main = "echo; echo Probing %s signals\n\n" % module + \
		   "for data in ${%s_data[@]}\n" % module + \
		   "do\n" + \
		   "    pin=`echo $data | cut -d ':' -f1`\n" + \
		   "    pad=`echo $data | cut -d ':' -f2`\n" + \
		   "    ngpio=`echo $data | cut -d ':' -f4`\n" + \
		   "    echo $ngpio > /sys/class/gpio/export 2>/dev/null\n" + \
		   "    omapconf set bit $pad 18 1>/dev/null 2>/dev/null\n" + \
		   "    printf \"%8s\" $pin\n" + \
		   "done\n" + \
		   "echo\n\n"

	gen_main += "while true;\n" + \
		    "do\n" + \
		    "    for data in ${%s_data[@]}\n" % module + \
		    "    do\n" + \
		    "#       pin=`echo $data | cut -d ':' -f1`\n" + \
		    "#        pad=`echo $data | cut -d ':' -f2`\n" + \
		    "#       gpio=`echo $data | cut -d ':' -f3`\n" + \
		    "        ngpio=`echo $data | cut -d ':' -f4`\n" + \
		    "#       addr=`echo $data | cut -d ':' -f5`\n" + \
		    "#       bit=`echo $data | cut -d ':' -f6`\n" + \
		    "        val=`cat /sys/class/gpio/gpio$ngpio/value`\n" + \
		    "        printf %8d $val \n" + \
		    "    done\n" + \
		    "    echo\n" + \
		    "done\n\n"

	gen_footer = "###########################################\n"

	# Generate the shell script
	gen_script = gen_header + gen_data + gen_main + gen_footer
	filename = ("./%s-probe.sh" % module).lower()
	print "Generating '%s' script..." % filename
	gen_file = open(filename, "w+")
	gen_file.write(gen_script)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Generate the final pad and delay data in the required format
def format_pad_regs(padconf, per_padconf):
	print "######## Pad data output ########"
	if (args.format == "uboot"):
		uboot_format_pad_regs(padconf, per_padconf)
	elif (args.format == "linux"):
		linux_format_pad_regs(padconf, per_padconf)
	elif (args.format == "bios"):
		bios_format_pad_regs(padconf, per_padconf)
	else:
		error("Format %s not suppported" % args.format)

def format_delay_regs(delayconf, per_delayconf):
	print "######## Delay data output ########"
	if (args.format == "uboot"):
		uboot_format_delay_regs(delayconf, per_delayconf)
	elif (args.format == "linux"):
		linux_format_delay_regs(delayconf, per_delayconf)
	elif (args.format == "bios"):
		bios_format_delay_regs(delayconf, per_delayconf)
	else:
		error("Format %s not suppported" % args.format)

def get_pin_info(val):
	slew_fast = (val >> 19) & 0x1
	inp_en = (val >> 18) & 0x1
	pulltype = (val >> 17) & 0x1
	pull_dis = (val >> 16) & 0x1
	pin = "PIN"
	if (inp_en):
		pin += "_INPUT"
	else:
		pin += "_OUTPUT"
	if (pull_dis == 0):
		if (pulltype):
			pin += "_PULLUP"
		else:
			pin += "_PULLDOWN"
	if (slew_fast):
		pin += " | SLEWCONTROL"
	return pin

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Generate output in u-boot format
def uboot_format_pad_regs(padconf, per_padconf):
	print "\nconst struct pad_conf_entry dra74x_core_padconf_array[] = {"
	for i in padconf:
		(pad_name, pin_name, addr, val, mode, delayinfo) = i

		muxmode = val & 0xf
		(method, data) = delayinfo
		if (method == "LEGACY"):
			extramode = ""
		elif (method == "VIRTUAL"):
			delaymode = data
			extramode = " | VIRTUAL_MODE%d" % delaymode
		elif (method == "MANUAL"):
			extramode = " | MANUAL_MODE"
		else:
			error("Invalid delay method %s" % method)

		pin_info = get_pin_info(val) + extramode
		pad_short = re.sub("CTRL_CORE_PAD_", "", pad_name)
		comment = (pad_short + "." + pin_name).lower()

		print "\t{ %s, (M%d | %s) },\t/* %s */" % (pad_short, muxmode, pin_info, comment)
	print "};\n"

def uboot_format_delay_regs(delayconf, per_delayconf):
	manual_del = []
	for i in delayconf:
		(pad_name, pin_name, regname, del_offset, man_name, adel, gdel) = i

		entry = (del_offset, adel, gdel, regname, pin_name, man_name)
		manual_del.append(entry)

	es_rev = args.revision.replace('.', '_')
	print "\nconst struct iodelay_cfg_entry dra742_es" + es_rev + "_iodelay_cfg_array[] = {"
	for entry in sorted(manual_del):
		print "\t{ 0x%04X, %5d, %5d },\t/* %s : %s - %s */" % entry
	print "};\n"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Generate output in linux format
def linux_format_pad_regs(padconf, per_padconf):
	print "&dra7_pmx_core {"
	for per in per_padconf:

		if (re.match("MMC.*", per) == None):
			warn("Only MMC padconf is recommended in kernel")

		# Get the mode from the first entry
		(pad_name, pin_name, addr, val, mode, delayinfo) = per_padconf[per][0]

		dtsnode = ("%s_pins_%s" % (per, mode)).lower()
		print ""
		print "\t%s: %s {" % (dtsnode, dtsnode)
		print "\t\tpinctrl-single,pins = <"
		for i in per_padconf[per]:
			(pad_name, pin_name, addr, val, mode, delayinfo) = i

			muxmode = val & 0xf
			(method, data) = delayinfo
			if (method == "LEGACY"):
				extramode = ""
			elif (method == "VIRTUAL"):
				delaymode = data
				extramode = " | MUX_VIRTUAL_MODE%d" % delaymode
			elif (method == "MANUAL"):
				extramode = " | MANUAL_MODE"
			else:
				warn("Invalid method %s" % method)

			pin_info = get_pin_info(val) + extramode
			pad_short = re.sub("CTRL_CORE_PAD_", "", pad_name)
			offset = addr - 0x4a003400
			comment = (pad_short + "." + pin_name).lower()

			print "\t\t\t0x%03X\t(%s | MUX_MODE%d)\t/* %s */" % (offset, pin_info, muxmode, comment)
		print "\t\t>;"
		print "\t};"
	print "};\n"


def linux_format_delay_regs(delayconf, per_delayconf):
	print "&dra7_iodelay_core {"
	for per in per_delayconf.keys():

		if (re.match("MMC.*", per) == None):
			warn("Only MMC delayconf is recommended in kernel")

		# Get the mode from the first entry
		(pad_name, pin_name, regname, del_offset, mode, adel, gdel) = per_delayconf[per][0]

		dtsnode = ("%s_iodelay_%s_conf" % (per, mode)).lower()
		print "\t%s: %s {" % (dtsnode, dtsnode)
		print "\t\tpinctrl-single,pins = <"
		for i in per_delayconf[per]:
			(pad_name, pin_name, regname, del_offset, mode, adel, gdel) = i

			print "\t\t\t0x%03X (A_DELAY(%d) | G_DELAY(%d))\t/* %s */" % (del_offset, adel, gdel, regname)
		print "\t\t>;"
		print "\t};"
	print "};\n"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Generate output in bios format
def bios_format_pad_regs(padconf, per_padconf):
	for i in padconf:
		(pad_name, pin_name, addr, val, mode, delayinfo) = i

		delaylist = []
		(method, data) = delayinfo
		if (method == "VIRTUAL"):
			virt_mode = data
			val = val | ( virt_mode<< 4)
			val = val | (1 << 8)
		elif (method == "MANUAL"):
			delaylist = data
			val = val | (1 << 8)

		offset = addr - 0x4a003400
		out_delreg = out_adel = out_gdel = 0
		in_delreg  = in_adel  = in_gdel  = 0
		oen_delreg = oen_adel = oen_gdel = 0

		for j in delaylist:
			(regname, adel, gdel) = j
			if (re.match(".*_IN", regname) != None):
				in_delreg = xml_iodelay_get_reg(regname)
				in_adel = adel
				in_gdel = gdel
			elif (re.match(".*_OEN", regname) != None):
				oen_delreg = xml_iodelay_get_reg(regname)
				oen_adel = adel
				oen_gdel = gdel
			elif (re.match(".*_OUT", regname) != None):
				out_delreg = xml_iodelay_get_reg(regname)
				out_adel = adel
				out_gdel = gdel
		indel  = "{ 0x%04X, %4d, %4d , 0}" % (in_delreg,  in_adel,  in_gdel)
		oendel = "{ 0x%04X, %4d, %4d , 0}" % (oen_delreg, oen_adel, oen_gdel)
		outdel = "{ 0x%04X, %4d, %4d , 0}" % (out_delreg, out_adel, out_gdel)
		pin_info = get_pin_info(val)
		print "\t/* %s -> %s (%s) %s delaymode */" % (pad_name, pin_name, pin_info, method)
		print "\t{ 0x%03X, 0x%08X, %s, %s, %s }" % (offset, val, indel, oendel, outdel)

def bios_format_delay_regs(delayconf, per_delayconf):
	debug("Manual delays are not configured separately")
	debug("Delays are configured as part of the padconf step only")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Main execution starts here
# * Parse all the XML files - Get the pad data and IOdelay data
# * Iterate over the pad register dumps
# * For each pad, find out the muxmode being used and the pad direction, etc
# * For the muxmode being used, find out available virtual/modes
#   - Dump the virtual/manual modes available (based on options)
#   - Build up the gpio script to read the pad data using GPIO module
# * Select the correct mode to be used
#   - Check if the mode is described for the peripheral module, (from selected-modes.txt)
#   - Otherwise ask the user (with a menu) to decide the delaymode
#     => Update the selection for the whole of the peripheral module
#   - Worst case, skip the delaymode for this pad
# * Build the pad and delay register information
# * Create the dump for pad and delay data in linux/u-boot format
# * Dump the delaymode selection for each peripheral
# * Dump the GPIO script for each peripheral (all pads grouped together)
#

# List of all the pads being configured
padconf = []
# List of all the delay registers being configured (for manual mode)
delayconf = []
# Dictionary of pads grouped by peripheral/module
per_padconf = {}
# Dictionary of delay registers grouped by peripheral/module
per_delayconf = {}

if(args.check_xml):
	xml_check_correctness(modehelp)
	exit()

# Start iterating over each pad
for i in range(0, 260):
	# Find out the muxmode and pad direction, etc
	addr = 0x4a003400 + i * 4
	offset = 0x1400 + i * 4
	dts_offset = offset - 0x1400

	if (addr not in reglist):
		warn("Register dump for pad 0x%X not found" % addr)
		continue
	val = regmap[addr]
	muxmode = val & 0xf

	# Find out the pin based on the muxmode
	pad_name = xml_pad_get_name(offset)
	pin_name = xml_model_get_signal(offset, muxmode)
	if (pad_name == "Unknown" or pin_name == "Unknown"):
		warn("Cannot find out Pad/Pin name for PAD address 0x%X (%s[%s] = %s)" \
		% (addr, pad_name, muxmode, pin_name))
		continue
	if (pin_name == "DRIVER"):
		continue

	# Find out if the delaymode for this module is already selected
	match = re.match("([^_]+)_.*", pin_name)
	if (match):
		module = match.groups(0)[0]
	else:
		module = pin_name

	if (args.module != "" and re.match("%s" % args.module, module) == None):
		continue

	# It is recommended to keep the reset value of the slewcontrol bit
	# Find out the reset value from XML and update if required
	if (args.resetslew):
		val |= (xml_pad_get_slew(offset) & 0x1) << 19

	debug("PAD: %s: Addr= 0x%08X Value = 0x%08X \"%s\"" \
		% (pad_name, addr, val, pin_name))

	# Find out all the possible virtual, manual modes fot the specific pad -> pin combination
	(virt, man) = xml_find_delaymodes(pad_name, muxmode)

	if (args.gpio == True):
		mode = "LEGACY"
	else:
		# Need to select one out of allowed modes
		mode = select_mode(pad_name, pin_name, module, virt, man, modehelp, sel)

	# Remember the selection for that module, for later reuse
	if (args.strict == False and module not in sel.keys()):
		sel[module] = mode

	if (mode == "SKIP"):
		continue
	elif (mode == "LEGACY"):
		delayinfo = ("LEGACY", "")
	elif (mode in virt):
		delayinfo = ("VIRTUAL", virt[mode])
	elif (mode in man):
		delayinfo = ("MANUAL", man[mode])
	else:
		error("Unknown mode '%s' selected" % mode)
		continue

	debug("  => Using mode %s" % mode)
	paddata = (pad_name, pin_name, addr, val, mode, delayinfo)
	if (module not in per_padconf.keys()):
		per_padconf[module] = []
	padconf.append(paddata)
	per_padconf[module].append(paddata)

	if(mode not in man):
		continue

	# Add the delay data if using manual mode
	for regval in man[mode]:
		(regname, adel, gdel) = regval
		del_offset = xml_iodelay_get_reg(regname)
		if (del_offset == 0):
			error("Can't find delay offset of register %s" % regname)
			break
		debug("  => Manual delay[%s] = (%d, %d)" % (regname, adel, gdel))
		delaydata = (pad_name, pin_name, regname, del_offset, mode, adel, gdel)
		if (module not in per_delayconf.keys()):
			per_delayconf[module] = []
		delayconf.append(delaydata)
		per_delayconf[module].append(delaydata)

if(args.gpio == True):
	pad_dump_gpio(padconf, per_padconf)
	exit()

format_pad_regs(padconf, per_padconf)
format_delay_regs(delayconf, per_delayconf)

if (args.interactive):
	# Summary of the generated data
	print ""
	print "Total PAD registers: %d" % len(padconf)
	print "Total DELAY registers: %d" % len(delayconf)
	print ""
	# Dump the final selection for reuse
	print "Selected modes for each peripheral module\n"
	for group in sel:
		print "%s,\t%s" % (group, sel[group])
	print ""
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
